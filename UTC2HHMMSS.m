function hora= UTC2HHMMSS(ano, mes, dia, UTC)
%fun��o que transforma  tempo UTC para o formato UTC
% a saida � uma string no formato HH:MM:SS
UTC = dadosGravados(:,38);
horaUTC = fix(UTC/3600000);
minutoUTC = fix(rem(UTC,3600000)/60);
segundoUTC = (rem (UTC,3600000)/60)-minutoUTC;
serialDateNum = datenum(ano, mes, dia, horaUTC, minutoUTC, segundoUTC);
hora = datestr(serialDateNum,13); 
end