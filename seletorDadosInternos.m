function [vetorInterno indiceExportado] = seletorDadosInternos (x,y,u)
% entre quatro pontos determinados por limites de x e y seleciona os que se
% encontram nos limites dos eixos x y. A sele��o � feita por pontos do tipo
% (x,y,n) onde n � a posi��o no vetor do ponto selecionado. A sa�da � dada
% por dois pontos intermedi�rios (que possuem o segundo e o terceiro
% valores, em ordem crescente, de n).

tamanhoVetorU = size(u);

vx1 = u(:,1)-x(1);
ax1 = min(abs(vx1));
nax1 = find(abs(vx1) == ax1);

vx2 = u(:,1)-x(2);
ax2 = min(abs(vx2));
nax2 = find(abs(vx2) == ax2);

v = u(nax1:nax2,:);

wy1 = v(:,2)-y(1);
indicesWy1 = encontraMudancaSinal(wy1);

wy2 = v(:,2)-y(2);
indicesWy2 = encontraMudancaSinal(wy2);

indices = sort([indicesWy1 indicesWy2]);
indiceExportado = [];
if v(1,2)-y(1)>0 & v(1,2)-y(2)<0
    if isempty(indices)
        vetorInterno = v;
        indiceExportado = [1:max(size(vetorInterno))];
    else
        if  v(end,2)-y(1)>0 & v(end,2)-y(2)<0
            vetorInterno = v(1:indices(1),:);
            indiceExportado = [1:indices(1)];
            contador = 2;
            while contador < max(size(indices))
                vetorInterno = [ vetorInterno; v(indices(contador):indices(contador+1),:)];
                indiceExportado = [indiceExportado [indices(contador):indices(contador+1)]];
                contador = contador + 2;
            end
            vetorInterno = [ vetorInterno; v(indices(contador):end,:)];
            indiceExportado = [indiceExportado [indices(contador):max(v)]];
        else
            vetorInterno = v(1:indices(1)-2,:);
            indiceExportado = [indiceExportado [1:indices(1)-2]];
            contador = 2;
            while contador < max(size(indices))
                vetorInterno = [ vetorInterno; v(indices(contador):indices(contador+1),:)];
                indiceExportado = [indiceExportado [indices(contador):indices(contador+1)]];
                contador = contador + 2;
            end
        end
    end
else
    if  v(end,2)-y(1)>0 & v(end,2)-y(2)<0
        vetorInterno = [];
        contador = 1;
        while contador < max(size(indices))
            vetorInterno = [ vetorInterno; v(indices(contador):indices(contador+1),:)];
            indiceExportado = [indiceExportado [indices(contador):indices(contador+1)]];
            contador = contador + 2;
        end
        vetorInterno = [ vetorInterno; v(indices(contador):end,:)];
        indiceExportado = [indiceExportado [indices(contador):max(v)]];
    else
        vetorInterno = [];
        contador = 1;
        while contador <= max(size(indices))
            vetorInterno = [ vetorInterno; v(indices(contador):indices(contador+1),:)];
            indiceExportado = [indiceExportado [indices(contador):indices(contador+1)]];
            contador = contador + 2;
        end
    end
end
indiceExportado = indiceExportado + nax1 -1;
end

function indices = encontraMudancaSinal(wy1)
sinalWy1 = sign(wy1);
indiceElementosNulos = find(~sinalWy1);%find(X) returns the linear indices corresponding to the nonzero entries of the array X
sinalWy1(indiceElementosNulos)=1;
indicesMudancaSinal = find([0 diff(sign(sinalWy1'))]~=0);% encontra a posi��o do elemento que mudou de sinal
if isempty(indicesMudancaSinal) == 0%True(=1) for empty array.
    if sinalWy1(indicesMudancaSinal(1)-1)==1
        indicesMudancaSinalIncio = indicesMudancaSinal(1:2:end)+1;
        indicesMudancaSinalFim = indicesMudancaSinal(2:2:end)-1;
    else
        indicesMudancaSinalIncio = indicesMudancaSinal(1:2:end);
        indicesMudancaSinalFim = indicesMudancaSinal(2:2:end)-1;
    end
    indices = sort([indicesMudancaSinalIncio indicesMudancaSinalFim]);
else
    indices = [];
end
end