function main

clc
clear all
% for i =1:7
%     h.acao01 = msgbox(['Insira o nome do arquivo que conter� os dados do teste contido na aba ' i ' da planilha ...'],...
%         'Selecionar arquivo', 'help');
%        waitfor(h.acao01);
%     nomeArquivo = input('entre com o nome do Arquivo:','s');
%     dadoEnsaio = xlsread('/Volumes/GUI/testesXHALE/LNCA-XHALE25112016/corridasTunel.xlsx',i);
%     %corrigir o vetor de dados
%     tamanhoVetor = max(size(dadoEnsaio));
%     dados=[];
%     for i = 1:2:tamanhoVetor
%         dados = [dados;dadoEnsaio(i,:)];
%     end
%     save([nomeArquivo '.mat'], 'dados');
% end
densidadeLaboratorio = 94270;%Pa
temperaturaLaboratorio = 273.15+25;%K
densidadeLaboratorio = densidadeLaboratorio/(temperaturaLaboratorio*287.0465);
for i=1:7
    indice = num2str(i);
    indice2 = input('', 's');
    nomeArquivoEagleTree = ['PARAMcorrida0' indice2 'ms.mat'];
    nomeArquivo = ['corrida0' indice 'Tratada.mat'];
    load(nomeArquivo); load(nomeArquivoEagleTree);
    %conversao dos dados para press�o
    pressao = calibraSonda(dados(:,4:5));
    velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
    velocidadeEagleTree = dadosGravados(:,14);
    plot(dados(:,1),velocidadeReferencia); hold on;
    plot(dadosGravados(:,38)-dadosGravados(1,38),velocidadeEagleTree);grid on;
    figure(i);
    subplot(3,2,1);plot(dados(:,1),dados(:,4));grid on;grid minor;
%     subplot(3,2,2);plot(dados(:,1),sqrt(abs(pressao(:,1)).*2./1.2754));grid on;grid minor;
    subplot(3,2,2);plot(dados(:,1),abs(pressao(:,1)));grid on;grid minor;
    subplot(3,2,3);plot(dados(:,1),dados(:,5));grid on;grid minor;
%     subplot(3,2,4);plot(dados(:,1),sqrt(abs(pressao(:,2)).*2./1.2754));grid on;grid minor;
    subplot(3,2,4);plot(dados(:,1),abs(pressao(:,2)));grid on;grid minor;
    subplot(3,2,5);plot(dados(:,1),dados(:,5)-dados(:,4));grid on;grid minor;
    subplot(3,2,6);plot(dados(:,1),abs(-pressao(:,1)+pressao(:,2)));grid on;grid minor;
%     subplot(3,2,6);plot(dados(:,1),sqrt(abs(-pressao(:,1)+pressao(:,2)).*2./1.2754));grid on;grid minor;
end
end

function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end

    