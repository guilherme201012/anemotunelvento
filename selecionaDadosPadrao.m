function main

clc
clear all
% for i =1:7
%     h.acao01 = msgbox(['Insira o nome do arquivo que conter� os dados do teste contido na aba ' i ' da planilha ...'],...
%         'Selecionar arquivo', 'help');
%        waitfor(h.acao01);
%     nomeArquivo = input('entre com o nome do Arquivo:','s');
%     dadoEnsaio = xlsread('/Volumes/GUI/testesXHALE/LNCA-XHALE25112016/corridasTunel.xlsx',i);
%     %corrigir o vetor de dados
%     tamanhoVetor = max(size(dadoEnsaio));
%     dados=[];
%     for i = 1:2:tamanhoVetor
%         dados = [dados;dadoEnsaio(i,:)];
%     end
%     save([nomeArquivo '.mat'], 'dados');
% end
densidadeLaboratorio = 94270;%Pa
temperaturaLaboratorio = 273.15+25;%K
densidadeLaboratorio = densidadeLaboratorio/(temperaturaLaboratorio*287.0465);
for i=1:7
    indice = num2str(i);
%     nomeArquivoEagleTree = ['PARAMcorrida0' indice '.mat'];
    nomeArquivo = ['corrida0' indice '.mat'];
    load(nomeArquivo); %load(nomeArquivoEagleTree);
    %conversao dos dados para press�o
    novosDados = dados;
    continuarDeletandoDados = 's';
        while continuarDeletandoDados == 's'
            pressao = calibraSonda(novosDados(:,4:5));
            velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
        %     velocidadeEagleTree = dadosGravados(:,14);
        %     plot(dados(:,1),velocidadeReferencia); hold on;
        %     plot(dadosGravados(:,38)-dadosGravados(1,38),velocidadeEagleTree);grid on;
            fig=figure(i);
        %     subplot(3,2,1);plot(dados(:,1),dados(:,4));grid on;grid minor;
        %     ylabel('coluna 04')
        % %     subplot(3,2,2);plot(dados(:,1),sqrt(abs(pressao(:,1)).*2./1.2754));grid on;grid minor;
        %     subplot(3,2,2);plot(dados(:,1),abs(pressao(:,1)));grid on;grid minor;
        %     ylabel('pressao 1')
        %     subplot(3,2,3);plot(dados(:,1),dados(:,5));grid on;grid minor;
        %     ylabel('coluna 05')
        % %     subplot(3,2,4);plot(dados(:,1),sqrt(abs(pressao(:,2)).*2./1.2754));grid on;grid minor;
        %     subplot(3,2,4);plot(dados(:,1),abs(pressao(:,2)));grid on;grid minor;
        %     ylabel('pressao 02')
        %     subplot(3,2,5);plot(dados(:,1),dados(:,5)-dados(:,4));grid on;grid minor;
        %     ylabel('coluna 05-04')
            deltaPressao = abs(-pressao(:,1)+pressao(:,2));
            velocidade = sqrt(2*deltaPressao/densidadeLaboratorio);
            figura01 = subplot(2,1,1);
            plot(novosDados(:,1),velocidade);grid on;grid minor;
            ylabel('velocidade')
            subplot(2,1,2);
            plot(novosDados(:,1),novosDados(:,4));grid on;grid minor;
            ylabel('est�tica')
        %     subplot(3,2,6);plot(dados(:,1),sqrt(abs(-pressao(:,1)+pressao(:,2)).*2./1.2754));grid on;grid minor;
            
%             dcm_obj = datacursormode(fig);
%             set(dcm_obj,'DisplayStyle','datatip',...
%             'SnapToDataVertex','off','Enable','on')
%             c_info = getCursorInfo(dcm_obj)
         
            retangulo = getrect(figura01);
            x = [retangulo(1) retangulo(1)+retangulo(3)]';
            y = [retangulo(2) retangulo(2)+retangulo(4)]';
            [dadosSelecionados, linha, coluna] = selecionadaDadosEnsaio (x,y,novosDados(:,1), velocidade);
            subplot(2,1,1);
            hold on; 
            selection = plot(dadosSelecionados(:,1),dadosSelecionados(:,2),'ro');
            resposta = input('gostaria de deletar os pontos selecionados?(s/n)', 's');
            if resposta =='s'
                if linha == 1 
                    novosDados = novosDados(coluna+1:end,:);
                else
                    if coluna == max(size(novosDados))
                        novosDados = novosDados(1:linha-1,:);
                    else
                        novosDados01 = novosDados(1:linha-1,:);
                        novosDados02 = novosDados(coluna+1:end,:);
                        novosDados = [novosDados01; novosDados02];
                end
                end
            hold off;
            pressao = calibraSonda(novosDados(:,4:5));
            velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
            deltaPressao = abs(-pressao(:,1)+pressao(:,2));
            velocidade = sqrt(2*deltaPressao/densidadeLaboratorio);
            subplot(2,1,1);
            plot(novosDados(:,1),velocidade);grid on;grid minor;
            ylabel('velocidade')
            subplot(2,1,2);
            plot(novosDados(:,1),novosDados(:,4));
            hold on;
            plot(novosDados(:,1),novosDados(:,5),'k');
            hold on;
            plot(novosDados(:,1),novosDados(:,5)-novosDados(:,4),'r');
            grid on;grid minor;
            ylabel('est�tica')
            continuarDeletandoDados = input('gostaria de selecionar novos pontos ?(s/n)', 's');
            save(['corrida0' num2str(i) 'Tratada.mat'], 'dados');
            else
                delete(selection);
    end
end
end
end

function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end

    