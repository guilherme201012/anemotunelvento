function padrao = determinaPadrao(dados)
% determina os valores padr�es de altitude e velocidade e com seus desvios
% padr�o com confian�a de 95%. Ao ser rodado apresenta o histograma com a
% curva normal fitada em cada um dos dois par�metros.
%%ENTRADA: dados
%vetor com cinco colunas 
pressaoLaboratorio = 94270;%Pa (verificar cart�o de voo com detalhes da coleta do dado)
temperaturaLaboratorio = 273.15+23.333;%K
densidadeLaboratorio = pressaoLaboratorio/(temperaturaLaboratorio*287.0465);
pressao = calibraSonda(dados(:,4:5));
deltaPressao = abs(-pressao(:,1)+pressao(:,2));
velocidade = sqrt(2*deltaPressao/densidadeLaboratorio);%ms
histfit(velocidade);pause;
padrao.velocidadeMedia = mean(velocidade);
padrao.velocidadeDesvio95 = 3*std(velocidade);
altitude = 0.3048*(145.442*(1-(pressao(:,1)*0.01/1013.25).^0.190263));%m
histfit(altitude );pause;
padrao.altitudeMedia = mean(altitude);
padrao.altitudeDesvio95 = 3*std(altitude);
end
    function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end