function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end