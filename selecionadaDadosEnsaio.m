function [dados, linha, coluna] = selecionadaDadosEnsaio (x,y,tempo, parametro)
% entre quatro pontos determinados por limites de x e y seleciona os que se
% encontram nos limites dos eixos x y. A sele��o � feita por pontos do tipo
% (x,y,n) onde n � a posi��o no vetor do ponto selecionado. A sa�da � dada
% por dois pontos intermedi�rios (que possuem o segundo e o terceiro
% valores, em ordem crescente, de n).
xy = encontraTempoNaVizinhanca(x,y,tempo,parametro);
linha = xy(1,3);coluna = xy(2,3);
% linha2 = yx(1,3);coluna2 = yx(2,3);
% [ordem, indice] = sort([coluna2  linha2  linha coluna]);
% linha = ordem(2);coluna = ordem(3);
dados = [tempo(linha(1):coluna(1)) ...
    parametro(linha(1):coluna(1))];
% yx = encontraTempoNaVizinhanca(y,x,dados(:,2), dados(:,1));
% linha = yx(1,3)+linha;coluna =  yx(2,3)+linha;
% % linha = ordem(2);coluna = ordem(3);
% dados = [tempo(linha(1):coluna(1)) ...
%     parametro(linha(1):coluna(1))];
% xy = encontraTempoNaVizinhanca(y,x,dados(:,2),dados(:,1));
% linha = xy(1,3);coluna = xy(2,3);
% dados = [dados(linha(1):coluna(1),2) dados(linha(1):coluna(1),1)];
end



function saida = encontraTempoNaVizinhanca(x,y, X, Y)
tamanhoVetorTempo = size(X);
tempoRef = x*ones(1,tamanhoVetorTempo(1));
refY = y*ones(1,tamanhoVetorTempo(1));
deltaX = X*ones(1,2) - tempoRef';
deltaY = Y*ones(1,2) - refY';
[valorMinModuloAx, nAx] = min(abs(deltaX(:,1))); 
[valorMinModuloBx, nBx] = min(abs(deltaX(:,2)));
[valorMinModuloAy, nAy] = min(abs(deltaY(:,1))); 
[valorMinModuloBy, nBy] = min(abs(deltaY(:,2)));
Ax = [X(nAx+1) Y(nAx+1) nAx+1];
Bx = [X(nBx-1) Y(nBx-1) nBx-1];
Ay = [X(nAy+1) Y(nAy+1) nAy+1];
By = [X(nBy-1) Y(nBy-1) nBy-1];
% Ay = [X(nAy) Y(nAy) nAy];
% By = [X(nBy) Y(nBy) nBy];
% if Ay(2)>By(2)
%     Ay = [X(nAy+1) Y(nAy+1) nAy+1];
%     Bx = [X(nBx-1) Y(nBx-1) nBx-1];
% else
%     By = [X(nBy-1) Y(nBy-1) nBy-1];
% end
pontoInternoSelecao=[];
if Ax(1)>=x(1) & Ax(1)<=x(2) & Ax(2)>=y(1) & Ax(2)<=y(2) 
    pontoInternoSelecao = [Ax];
end
if Bx(1)>=x(1) & Bx(1)<=x(2) & Bx(2)>=y(1) & Bx(2)<=y(2) 
    pontoInternoSelecao = [pontoInternoSelecao;Bx];
end
if Ay(1)>=x(1) & Ay(1)<=x(2) & Ay(2)>=y(1) & Ay(2)<=y(2) 
    pontoInternoSelecao = [pontoInternoSelecao;Ay];
end
if By(1)>=x(1) & By(1)<=x(2) & By(2)>=y(1) & By(2)<=y(2) 
    pontoInternoSelecao = [pontoInternoSelecao;By];
end
pontosSelecionados = sortrows(pontoInternoSelecao,3);
saida = pontosSelecionados;
% if pontosSelecionados(1,2) < 0
%     xInicialRequerido = X(pontosSelecionados(1,1)+1);
%     yInicialRequerido = Y(pontosSelecionados(1,1)+1);
% else
%     xInicialRequerido = X(pontosSelecionados(1,1));
%     yInicialRequerido = Y(pontosSelecionados(1,1));
% end    
% if pontosSelecionados(2,2) < 0
%     xFinalRequerido = X(pontosSelecionados(2,1));
%     yFinalRequerido = Y(pontosSelecionados(2,1));
% %     if yFinalRequerido < y(2)
% %         yFinalRequerido = y(2);
% %     end
% else
%     xFinalRequerido = X(pontosSelecionados(2,1)-1);
%     yFinalRequerido = Y(pontosSelecionados(2,1)-1);
% %     if yFinalRequerido < y(2)
% %         yFinalRequerido = y(2);
% %     end
% end    
% saida = [xInicialRequerido yInicialRequerido pontosSelecionados(1,1) ;
%          xFinalRequerido   yFinalRequerido   pontosSelecionados(2,1)];
end