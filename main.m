function main
clc
clear all
pressaoLaboratorio = 94270;%Pa (verificar cart�o de voo com detalhes da coleta do dado)
temperaturaLaboratorio = 273.15+23.333;%K
densidadeLaboratorio = pressaoLaboratorio/(temperaturaLaboratorio*287.0465);
for i=2:7
    indice = num2str(i);
    nomeArquivo = ['corrida0' indice '.mat'];
    load(nomeArquivo);
    novosDados = dados;
    continuarDeletandoDados = 's';
    while continuarDeletandoDados == 's'
        pressao = calibraSonda(novosDados(:,4:5));
        velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
        fig=figure(i);
        deltaPressao = abs(-pressao(:,1)+pressao(:,2));
        velocidade = sqrt(2*deltaPressao/densidadeLaboratorio);
        altitude = 0.3048*(145.442*(1-(pressao(:,1)*0.01/1013.25).^0.190263));%m
        %PLOTAGEM DE DADOS
        figura(1) = subplot(2,1,1);
        plot(novosDados(:,1),velocidade);grid on;grid minor;
        ylabel('velocidade[m/s]')
        figura(2) = subplot(2,1,2);
        plot(novosDados(:,1),altitude);grid on;grid minor;
        ylabel('altitude[m]')
        %SELETOR DE DADOS
        retangulo = getrect(figura(1));
        x = [retangulo(1) retangulo(1)+retangulo(3)]';
        y = [retangulo(2) retangulo(2)+retangulo(4)]';
        [dadosSelecionados indices] = seletorDadosInternos (x,y,...
            [novosDados(:,1) velocidade altitude]);
        subplot(2,1,1);hold on; 
        selection = plot(dadosSelecionados(:,1),dadosSelecionados(:,2),'ro');
        subplot(2,1,2);hold on; 
        selection = plot(dadosSelecionados(:,1),dadosSelecionados(:,3),'ro');
        xlim([min(x)-.2 max(x)+.2]);
        linkaxes(figura,'x');
        uiwait(msgbox('Zomm aplicado na Figura','Zomm'));
        xlim([min(novosDados(:,1))-.2 max(novosDados(:,1))+.2]);
        linkaxes(figura,'x');
        resposta = input('gostaria de deletar os pontos selecionados?(s/n)', 's');
        if resposta =='s'
            contador = 0;
            for j = 1:max(size(indices))
                novosDados(indices(j)-contador,:) = [];
                contador = contador+1;
            end
            close(figure(i));
            pressao = calibraSonda(novosDados(:,4:5));
            velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
            fig=figure(i);
            deltaPressao = abs(-pressao(:,1)+pressao(:,2));
            velocidade = sqrt(2*deltaPressao/densidadeLaboratorio);
            altitude = 0.3048*(145.442*(1-(pressao(:,1)*0.01/1013.25).^0.190263));%m
            %PLOTAGEM DE DADOS
            figura(1) = subplot(2,1,1);
            plot(novosDados(:,1),velocidade);grid on;grid minor;
            ylabel('velocidade[m/s]')
            figura(2) = subplot(2,1,2);
            plot(novosDados(:,1),altitude);grid on;grid minor;
            ylabel('altitude[m]')
            desvioPadraoVelocidade = std(velocidade);
            mediaVelocidade = mean(velocidade);
            subplot(2,1,1);hold on;
            plot(novosDados(:,1),ones(max(size(novosDados(:,1))),1)*mediaVelocidade,'-. r');hold on;
            plot(novosDados(:,1),ones(max(size(novosDados(:,1))),1)*(mediaVelocidade-desvioPadraoVelocidade),'- k');hold on;
            plot(novosDados(:,1),ones(max(size(novosDados(:,1))),1)*(mediaVelocidade+desvioPadraoVelocidade),'- k');
            grid minor;
            end
            continuarDeletandoDados = input('gostaria de deletar outros pontos?(s/n)', 's');
    end
    nomeArquivo = input('Insira o nome do arquivo:','s');
    nomeArquivo = [nomeArquivo '.mat'];
    dados = novosDados;
    save(nomeArquivo, 'dados');
end
end
        
function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end