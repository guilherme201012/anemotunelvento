function padrao = selecionaETS(dadosGravados)
figura(1) = subplot(2,1,1);
velocidade = dadosGravados(:,14).*.514444;%m/s
plot(dadosGravados(:,38),velocidade);grid on;grid minor;
ylabel('velocidade[m/s]')
figura(2) = subplot(2,1,2);
altitude = dadosGravados(:,9);% ainda n�o sabemos a unidade
plot(dadosGravados(:,38),altitude);grid on;grid minor;
ylabel('altitude[m]')
%SELETOR DE DADOS
tempoInicial = input('HH:MM:SS do in�cio da aquisi��o: ','s');
tempoInicial = strsplit(tempoInicial);
tempoInicial = 3600000*(str2num(tempoInicial{1}(1))*10+str2num(tempoInicial{1}(2)))+...
    60000*(str2num(tempoInicial{1}(4))*10+str2num(tempoInicial{1}(5)))+...
    1000*(str2num(tempoInicial{1}(7))*10+str2num(tempoInicial{1}(8)));
tempoFinal = input('HH:MM:SS do final da aquisi��o: ','s'); 
tempoFinal = strsplit(tempoFinal);
tempoFinal = 3600000*(str2num(tempoFinal{1}(1))*10+str2num(tempoFinal{1}(2)))+...
    60000*(str2num(tempoFinal{1}(4))*10+str2num(tempoFinal{1}(5)))+...
    1000*(str2num(tempoFinal{1}(7))*10+str2num(tempoFinal{1}(8)));
subplot(2,1,1);hold on;
plot(tempoInicial.*ones(1,2), [min(velocidade) max(velocidade)],'g', 'linewidth', 2);
hold on;
plot(tempoFinal.*ones(1,2), [min(velocidade) max(velocidade)],'r', 'linewidth', 2);
retangulo = getrect(figura(1));
x = [retangulo(1) retangulo(1)+retangulo(3)]';
y = [retangulo(2) retangulo(2)+retangulo(4)]';
[dadosSelecionados indices] = seletorDadosInternos (x,y,[dadosGravados(:,38)...
                              velocidade altitude...
                              [dadosGravados(:,1:8) dadosGravados(:,10:13)...
                              dadosGravados(:,15:37) dadosGravados(:,39:end)]]);
xlim([tempoInicial-2000 tempoFinal+2000]);
linkaxes(figura,'x');
subplot(2,1,1);hold on; 
selection = plot(dadosSelecionados(:,1),dadosSelecionados(:,2),'ro');
subplot(2,1,2);hold on; 
selection = plot(dadosSelecionados(:,1),dadosSelecionados(:,3),'ro');
velocidade = dadosSelecionados(:,2);
altitude = dadosSelecionados(:,3);
figure(2)
histfit(velocidade);pause;
padrao.velocidadeMediaETS = mean(velocidade);
padrao.velocidadeDesvio95ETS = 3*std(velocidade);
histfit(altitude );pause;
padrao.altitudeMediaETS = mean(altitude);
padrao.altitudeDesvio95ETS = 3*std(altitude);
end