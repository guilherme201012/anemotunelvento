function dados = leitorLogFile(endereco)
file = fopen(endereco);
dados = textscan(file,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ');
dados = cell2mat(dados);
dados = dados(end, :);
fclose(file);
end
% % ORDEM DOS PARAMETROS MEDIDOS
% 1 Time 
% 2 IsEvent 
% 3 EventError 
% 4 EventData 
% 5 Aileron 
% 6 Elevator 
% 7 Throttle 
% 8 Rudder 
% 9 Altitude1 
% 10 RPM 
% 11 RPM2
% 12 RPM3 
% 13 RPM4 
% 14 Speed 
% 15 Battery 
% 16 TempA 
% 17 TempB 
% 18 ackCurrent 
% 19 PackVoltage 
% 20 GForceX 
% 21 GForceY 
% 22 GForceA
% 23 GForceB 
% 24 ThermoA 
% 25 ThermoB 
% 26 ThermoC 
% 27 ThermoD 
% 28 ThermoE 
% 29 ThermoF 
% 30 SignalStrength 
% 31 PacketPercent
% 32 Latitude 
% 33 Longitude 
% 34 Altitude 
% 35 GPSSpeed 
% 36 Course 
% 37 Distance 
% 38 UTC 
% 39 NumSats 
% 40 GPS_Flags 
% 41 ADA 
% 42 ADB 
% 43 ADC
% 44 ADD 
% 45 ADE 
% 46 ADF 
% 47 ADG  
% 48 ServoCurrent 
% 49 RX_Holds 
% 50 RX_LostFrames 
% 51 RX_AntennaAFades 
% 52 RX_AntennaBFades
% 53 RX_AntennaLeftFades 
% 54 RX_AntennaRightFades