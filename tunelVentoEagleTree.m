function tunelVentoEagleTree
uiload;

HH = input('entre com o valore de HH do ponto:');
MM = input('entre com o valore de MM do ponto:');
SS = input('entre com o valore de SS do ponto:');
inclinacaoPitot = input('entre com a inclinacao do Pitot(\^circ):');

tempoUTC = (HH*3600+MM*60+SS)*1000;
[velocidadeCrescente,posicao] = unique(dadosGravados(:,14));
vetorDados = [dadosGravados(posicao,38) velocidadeCrescente dadosGravados(posicao,5)];
vetorDados = sortrows(vetorDados);
velocidadePontoEnsaio = interp1(vetorDados(:,1),vetorDados(:,2),tempoUTC);
altitudePontoEnsaio = interp1(vetorDados(:,1),vetorDados(:,3),tempoUTC);

plote(1) = subplot(2,2,1);plot(dadosGravados(:,38),dadosGravados(:,14));
hold on;
plot([tempoUTC;tempoUTC],[velocidadePontoEnsaio-2; velocidadePontoEnsaio+2],...
    'r', 'linewidth',2);grid on; grid minor;

plote(2) = subplot(2,2,3);plot(dadosGravados(:,38),dadosGravados(:,14));
hold on;
plot([tempoUTC;tempoUTC],[velocidadePontoEnsaio-2; velocidadePontoEnsaio+2],...
    'r', 'linewidth',2);grid on; grid minor;

plote(3) = subplot(2,2,2);plot(dadosGravados(:,38),dadosGravados(:,5));
hold on;
plot([tempoUTC;tempoUTC],[altitudePontoEnsaio-5; altitudePontoEnsaio+5],...
    'r', 'linewidth',2);grid on; grid minor;

linkaxes(plote,'x');
end