clc
clear all
h.acao01 = msgbox(['selecione o arquivo ".mat" que contenha os dados do ensaio do sistema EagleTree',char(174),' ...'],...
    'Selecionar arquivo', 'help');
   waitfor(h.acao01);
uiload;
%refer�ncia de Zp
ZpInicioTeste = input('entre com o valor da altitude no in�cio do teste(ft):');
ZpFimTeste = input('entre com o valor da altitude no fim do teste(ft):');
ZpReferencia = ZpInicioTeste*.5+ZpFimTeste*.5;
%plot dos dados de interesse
grafico(1) = subplot (2,1,1); plot(dadosGravados(:,38), dadosGravados(:,14));
grid on; grid minor;
ylabel('kt');
grafico(2) = subplot (2,1,2);plot(dadosGravados(:,38), dadosGravados(:,9));
grid on; grid minor;
ylabel('ft');
%geracao de dados obtidos com a druke
resposta = 's';druke=[];i=1;vetorStd=[];vetorFlag=[];vetorErro=[];flagVelocidade=[];
flagAltitude=[];
while resposta =='s'
   h.acao02 = msgbox('click entre os extremos que deseje dar zoom ...','guia de uso', 'help');
   waitfor(h.acao02);
   [x y]= ginput(2);
   xlim([min(x)-.2 max(x)+.2]);
   linkaxes(grafico,'x');
   h.acao03 = msgbox('selecione o valor setado durante o teste da Druke de velocidade e altitude ...'...
       ,'guia de uso', 'help');
   waitfor(h.acao03);
   valorVelocidadeDruke = input('entre com o valor setado na DRUKE de velocidade (kt): ');
   valorAltitudeDruke = input('entre com o valor setado na DRUKE de altitude (ft): ');
   valorAltitudeSistema = valorAltitudeDruke - ZpReferencia;
   h.acao04 = msgbox('selecione o trecho em que o teste foi realizado na Druke...'...
       ,'guia de uso', 'help');
   waitfor(h.acao04);
   [x,y] = ginput(2);
   velocidadeEagleTree = selecionadaDadosEnsaio (x,y,dadosGravados(:,38), dadosGravados(:,14));
   erroVelocidade = velocidadeEagleTree(:,2) - valorVelocidadeDruke;
   flagVelocidade = [ flagVelocidade;min(velocidadeEagleTree(:,2))*ones(2,1) [0; max(erroVelocidade)+1] ]; 
   flagVelocidade = [ flagVelocidade; max(velocidadeEagleTree(:,2))*ones(2,1) [0; max(erroVelocidade)+1] ];
   stdVelocidade = std(erroVelocidade);mediaVelocidade = mean(erroVelocidade);
   altitudeEagleTree = selecionadaDadosEnsaio (x,y,dadosGravados(:,38), dadosGravados(:,9));
   erroAltitude = altitudeEagleTree(:,2) - valorVelocidadeDruke;
   flagAltitude = [ flagAltitude; min(altitudeEagleTree(:,2))*ones(2,1) [0; max(erroAltitude)+1] ]; 
   flagAltitude = [ flagAltitude; max(altitudeEagleTree(:,2))*ones(2,1) [0; max(erroAltitude)+1] ];
   stdAltitude = std(erroAltitude);mediaAltitude = mean(erroAltitude);
   vetorStd = [vetorStd; i mediaVelocidade stdVelocidade mediaAltitude stdAltitude];
   vetorFlag =[vetorFlag; flagVelocidade flagAltitude];
   vetorErro =[vetorErro;erroVelocidade erroAltitude];
   i=i+1;
   drukeTemporario = [x valorVelocidadeDruke*ones(2,1) valorAltitudeSistema*ones(2,1)];
   druke = [druke; drukeTemporario];
   hold on;
   subplot (2,1,1); plot(druke(:,1), druke(:,2),'r', 'linewidth', 2);
   subplot (2,1,2);hold on; plot(druke(:,1), druke(:,3),'r', 'linewidth', 2);
   xlim([min(dadosGravados(:,38))-.2 max(dadosGravados(:,38))+.2]);
   h.acao05 = msgbox('informe se h� mais dados para ser inserido...'...
       ,'guia de uso', 'help');
   waitfor(h.acao05);
   resposta = input('Ainda deseja inserir mais dados do ensaio com a Druke (s/n)?','s');
end

% save('dadosEnsaio.mat', 'vetorStd', 'vetorFlag', 'vetorErro', 'druke')

% horaUTCDruke = input()
% %montar base de tempo
% ano = input('entre com o ano do ensaio: ');
% mes = input('entre com o m�s do ensaio: ');
% dia = input('entre com o dia do ensaio: ');
% UTC = dadosGravados(:,38);
% horaUTC = fix(UTC/3600000);
% minutoUTC = fix(rem(UTC,3600000)/60000);
% segundoUTC = (rem (UTC,3600000)/60000)-minutoUTC;
% serialDateNum = datenum(ano, mes, dia, horaUTC, minutoUTC, segundoUTC);
% horaEixoOx = datestr(serialDateNum,13);
% 
% %plot de dados
% plot(dadosGravados(:,38), dadosGravados(:,14));
% % set(gca,'xticklabel',horaEixoOx)
% figure();
% plot(dadosGravados(:,14));
% set(gca,'xticklabel',horaEixoOx)