function erroAnemometrico
uiload; %carregar arquivo de dados de ensaio em t�nel tratado
padrao = determinaPadrao(dados);
uiload; %carregar arquivo de dados do sistema Eagle Tree
erros = [];
resposta ='s';
while resposta == 's'
    angulo = input('entre com o valor de inclina��o do pitot(graus): ','s');
    angulo = str2num(angulo);
    padraoETS = selecionaETS(dadosGravados);
    erro.velocidade = padraoETS.velocidadeMediaETS-padrao.velocidadeMedia;
    erro.velocidadeDesvio = padraoETS.velocidadeDesvio95ETS+padrao.velocidadeDesvio95;
    erro.altitude = padraoETS.altitudeMediaETS-padrao.altitudeMedia;
    erro.altitudeDesvio = padraoETS.altitudeDesvio95ETS+padrao.altitudeDesvio95;
    erros = [erros; 
        angulo erro.velocidade erro.velocidadeDesvio erro.altitude erro.altitudeDesvio];
    save('erros.mat','erros');
    resposta = input('gostaria de continuar a determinar novos erros?(s/n)','s');
    close all;
end
figure(3);
subplot(1,2,1);errorbar(erros(:,1),erros(:,2),erros(:,3),'s','MarkerSize',10,...
    'MarkerEdgeColor','black','MarkerFaceColor','red');grid minor;
subplot(1,2,2);errorbar(erros(:,1),erros(:,4),erros(:,5),'s','MarkerSize',10,...
    'MarkerEdgeColor','black','MarkerFaceColor','red');grid minor;
end