function determinarErroPosicaoPitot
pressaoLaboratorio = 94270;%Pa (verificar cart�o de voo com detalhes da coleta do dado)
temperaturaLaboratorio = 273.15+23.333;%K
densidadeLaboratorio = pressaoLaboratorio/(temperaturaLaboratorio*287.0465);
sigmaDensidadeLab = densidadeLaboratorio*sqrt((0.05./pressaoLaboratorio).^2+...
    (0.5./temperaturaLaboratorio).^2 - ((0.05*0.5)./(pressaoLaboratorio.*temperaturaLaboratorio)));
for i=1:10
    switch i
        case 1 
            j =0;
        case 2 
            j =5;
        case 3 
            j =10;
        case 4 
            j =15;
        case 5 
            j =25;
        case 6 
            j =35;
        case 7 
            j =45;
        case 8 
            j =-5;
        case 9 
            j =-10;
        case 10 
            j =-15;
    end
    nomeArquivo = ['listaParametrosAoA' num2str(j) '.mat'];
    load(nomeArquivo);
    Vbas = sqrt((.9547./densidadeLaboratorio).*listaParametros(:,3).^2-34.4395./densidadeLaboratorio);
    Vbas = sqrt((1.5710./densidadeLaboratorio).*listaParametros(:,3).^2+44.3377./densidadeLaboratorio);
    Vbas = listaParametros(:,3);
    dVbas_drho = .5./Vbas.*((-.9547.*listaParametros(:,3).^2+34.4395)./densidadeLaboratorio.^2);
    dVbas_drho = .5./Vbas.*((1.5710.*listaParametros(:,3).^2+44.3377)./densidadeLaboratorio.^2);
    dVbas_d_VETS = .5./Vbas.*(2.*.9547.*listaParametros(:,3)/densidadeLaboratorio);
    dVbas_d_VETS = .5./Vbas.*(2.*1.5710.*listaParametros(:,3)/densidadeLaboratorio);
    sigmaVbas = sqrt(2.*sigmaDensidadeLab.^2.*dVbas_drho.^2+dVbas_d_VETS.^2.*listaParametros(:,7).^2);
     sigmaVbas =  listaParametros(:,7);
    sigmaVref3 = 3*(listaParametros(:,9)).*listaParametros(:,5).^2;
    sigmaDeltaBas = 2*Vbas.*sigmaVbas+2*listaParametros(:,5).*listaParametros(:,9);
    delta_bas = (Vbas.^2-listaParametros(:,5).^2)
    figure(1)
%     plot(Vbas.^4,listaParametros(:,5).^2,'o');hold on
%     u1 = Vbas.^2;
%     u2= delta_bas;
%     plot(Vbas, listaParametros(:,5),'s')
    plot((Vbas),(delta_bas),'s')
 
%     plot((Vbas),(delta_bas),'o')

%     plot((Vbas),listaParametros(:,5),'s');%hold on
%     errorbar(Vbas,listaParametros(:,5).^3,sigmaVref3,'o')
    errorbar(Vbas,delta_bas,2*Vbas.*sigmaVbas+2*listaParametros(:,5).*listaParametros(:,9),'o')
%     p = polyfit((Vbas(2:end)),(delta_bas(2:end)),5);
%     x1 = linspace(Vbas(2),Vbas(end));
%     y1 = polyval(p,x1);
%     hold on
%     plot(x1,y1)
%     plot(Vbas,log(delta_bas),'s');%hold on
%     plot(Vbas,log(delta_bas.^2),'s');%hold on
%     plot(Vbas,log(delta_bas.^4),'>');%hold on
%     plot(Vbas,log(delta_bas.^8),'*');%hold on
%     plot(log(Vbas.^2),log(delta_bas),'s');%hold on
%     plot(Vbas,log(delta_bas.^2),'s');%hold on
%     plot(Vbas,log(delta_bas.^4),'>');%hold on
%     plot(Vbas,log(delta_bas.^8),'*');%hold on
%     plot(Vbas.^8,delta_bas,'o');%hold on
%     plot(Vbas,delta_bas,'s');%hold on
%     plot(Vbas,delta_bas.^2,'>');%hold on
%     plot(Vbas,delta_bas.^4,'*');%hold on
%     plot(Vbas,delta_bas.^8,'o');%hold on
%     plot(Vbas.^8,delta_bas,'o');%hold on
%     plot(u1(2:end),u2(2:end),'o');hold on
    
    

%     plot(Vbas(3:end).^2,listaParametros(3:end,5).^2,'go')
%     semilogy(Vbas(2:end).^2,listaParametros(2:end,5).^2,'bo')
%     [linearizada, dadosLinearizacao,saida] = fit( Vbas(3:end).^2,...
%         listaParametros(3:end,5).^2,  'poly1' )
        if j~= -5 & j~= -10 & j~= -15  
            
%          [linearizada, dadosLinearizacao,saida] = fit( Vbas.^2,...
%             delta_bas,  'poly1' )
        
%         
%         ft = fittype('a*sin(c*x)',...
%     'dependent',{'y'},'independent',{'x'},...
%     'coefficients',{'a','c'});
        ft = fittype('poly3');
%         [linearizada, dadosLinearizacao,saida] = fit( (Vbas(1:end)),listaParametros((1:end),5).^3,...
%     ft)
        [linearizada, dadosLinearizacao,saida] = fit( Vbas,delta_bas,ft)
        hold on;
%         plot(linearizada, (Vbas(1:end)),listaParametros(1:end,5).^3)
        plot(linearizada, Vbas,delta_bas)
        cabecalho = {'x', 'y', 'errorx', 'errory'};
        VbasVsDeltaqBas = [ Vbas, delta_bas, sigmaVbas,sigmaDeltaBas];
%         Vbas2VsVref3 = [cabecalho; Vbas2VsVref3];
        nomeArquivo = ['VbasVsDeltaqBas_' num2str(j) '.dat'];
        fmt = repmat('%s\t ', 1, length(cabecalho));
        fmt(end:end+1) = '\n';
        fid = fopen(nomeArquivo, 'w');
        fprintf(fid, fmt, cabecalho{:});
        fclose(fid);
        dlmwrite(nomeArquivo,Vbas2VsVref3,'-append','delimiter','\t','precision',5)
        residuos = [ Vbas(1:end), saida.residuals]
        nomeArquivo = ['ResiduosVbasVsVref3' num2str(j) '.dat'];
        dlmwrite(nomeArquivo,residuos,'delimiter',' ','precision',5)
        else
        Vbas2VsVref3 = [ Vbas, listaParametros((1:end),5).^3, sigmaVbas,sigmaVref3];
%         nomeArquivo = ['Vbas2VsVref3_' num2str(j) '.dat'];
        dlmwrite(nomeArquivo,Vbas2VsVref3,'delimiter',' ','precision',5)   
        end
%     p1 = linearizada.p1
%     erroP1 = p1 - .9547./densidadeLaboratorio
%     p2 = linearizada.p2
%     delta_b = -(p2.*densidadeLaboratorio+34.4395).*.5
%     Vtrue2VsVbas2 = [ Vbas.^2, listaParametros(:,5).^2] 
%     Vbas2Vsdelta_bas = [ Vbas.^2, delta_bas]
    
%     delta_b = (Vbas.^2-listaParametros(:,5).^2)
%     figure(2);hold on;
%     plot(Vbas.^2,delta_b,'o')
    
end