function padrao = padroes
densidadeLaboratorio = 94270;%Pa
temperaturaLaboratorio = 273.15+25;%K
densidadeLaboratorio = densidadeLaboratorio/(temperaturaLaboratorio*287.0465);
for i=1:7
    nomeArquivo = ['corrida0' num2str(i) 'Tratada.mat'];
    load (nomeArquivo);
    pressao = calibraSonda(dados(:,4:5));
    velocidadeReferencia = sqrt(2*abs(-pressao(:,1)+pressao(:,2))/densidadeLaboratorio);
    velocidadeMedia = mean(velocidadeReferencia);
    altitudeReferencia = 145442*(1-(pressao(:,2)*0.01/101320).^(0.190263))*.3048;
    altitudeMedia = mean(altitudeReferencia);
    padrao = ['dados0' indice];
    padrao.velocidade = velocidadeMedia;
    padrao.altitude = altitudeMedia;
    padrao.vetor = [altitudeReferencia velocidadeReferencia];
    save(['dados0' num2str(i) 'Padrao.mat'], 'padrao');
end
end

function pressao = calibraSonda(dados)
    pressaoSonda01mmH2O = 6.6574*dados(:,1)-3.4903; %mmH2O
    pressaoSonda01Nm2 = pressaoSonda01mmH2O*9.80638; %Pa 
    pressaoSonda02mmH2O = -4.1949*dados(:,2)+14.852; %mmH2O
    pressaoSonda02Nm2 = pressaoSonda02mmH2O*9.80638; %Pa 
    pressao = [pressaoSonda01Nm2 pressaoSonda02Nm2];
end