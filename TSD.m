function varargout = TSD(varargin)
% TSD MATLAB code for TSD.fig
%      TSD, by itself, creates a new TSD or raises the existing
%      singleton*.
%
%      H = TSD returns the handle to a new TSD or the handle to
%      the existing singleton*.
%
%      TSD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TSD.M with the given input arguments.
%
%      TSD('Property','Value',...) creates a new TSD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TSD_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TSD_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TSD

% Last Modified by GUIDE v2.5 19-Apr-2017 22:14:58

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TSD_OpeningFcn, ...
                   'gui_OutputFcn',  @TSD_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TSD is made visible.
function TSD_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TSD (see VARARGIN)

% Choose default command line output for TSD
handles.output = hObject;
set(handles.axes1,'Visible','on');
axes(handles.axes1);grid on;
% im=imread('imagem.png');
% axes('position',[0.35,0.35,0.45,0.45])
% imshow(im)
set(handles.axes2,'Visible','off');
set(handles.axes3,'Visible','off');
set(handles.text4,'String','status:'); 
set(handles.text8,'String','ALARMES:');
set(handles.text7,'String','aquisi��o(s):');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TSD wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TSD_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiload
handles.vetor = dadosGravados;
for k = 1:max(size(dadosGravados(1,:)))
    string{k} = num2str(k);
end
set(handles.popupmenu3,'String',string);
guidata(hObject, handles);


% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.



% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selecionado = get(hObject,'Value');
lista = get(hObject,'String');
numeroPlots = lista{selecionado};
handles.parametosParaPlotar = numeroPlots;
switch numeroPlots
    case ''
        caixaMensagem = msgbox(['selecione a quantidade de par�metros /n que' ...
        'deseja visualizar']);uiwait(caixaMensagem);
    case '1'
        set(handles.axes1,'Visible','on'); axes(handles.axes1);grid on;
        set(handles.axes2,'Visible','off');
        set(handles.axes3,'Visible','off');
    case '2'
        set(handles.axes1,'Visible','off');
        set(handles.axes2,'Visible','on'); axes(handles.axes2);grid on;
        set(handles.axes3,'Visible','on'); axes(handles.axes3);grid on;
end
handles.flagEncerrar = 'false';
guidata(hObject, handles);
        


function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function plotaVetorTimeline(portaCOM,handles)

if(~exist('flag','var'))
    potenciometro = inicializaComSerial(portaCOM,handles);
end

buf_len = 100;
dadosGravados=[];
index = 1:buf_len;
dados = zeros(buf_len,3);
flagEncerrar = 0;
while (flagEncerrar == 0)
    flagEncerrar = conexaoPorta;
    if flagEncerrar == 0
        dado = leDados(potenciometro);
        cla;
        dadosGravados = [dadosGravados; dado];
        dados = [dados(2:end,:);dado];
        plotar(handles,dados,index);
        seletorPausa = get(handles.togglebutton2, 'Value');
        while (seletorPausa == 1)
           flagEncerrar = conexaoPorta;
           if flagEncerrar == 0
               seletorPausa = get(handles.togglebutton2, 'Value');
               plotar(handles,dados,index);%Matlab apparently is not able to get the status update from "stop callback", perhaps because too busy with simulating the intensive "simulation_loop" called from "run_Callback".
    % 
    % However I found a trick to give Matlab time to check stop_Callback updates.
    % The code example above works if a statement like: pause(0.000001) is placed among run_Callback instructions.
    % 
    % (I found this trick in another thread, not my idea; at least in my case it works)
    %      drawnow
    % Instead of "pause(0.0001)", use "drawnow;". "pause" command is extremely slow because it uses very inaccurate timing mechanism. If you're running a time-critical loop, NEVER use "pause" in your loop. Having said that, drawnow is not a perfect choice either. However, there is no delay. Only the overhead to process "drawnow" exists. So, if you're not running many GUI objects doing this and that, "drawnow" exits fairly well. In a very fast computer you may finish "drawnow" within 0.1ms, which is good for usual task.
        
           end
        end
    end
end
save('PARAM.mat','dadosGravados');


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
