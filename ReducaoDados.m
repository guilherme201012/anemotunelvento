function varargout = ReducaoDados(varargin)
% REDUCAODADOS MATLAB code for ReducaoDados.fig
%      REDUCAODADOS, by itself, creates a new REDUCAODADOS or raises the existing
%      singleton*.
%
%      H = REDUCAODADOS returns the handle to a new REDUCAODADOS or the handle to
%      the existing singleton*.
%
%      REDUCAODADOS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REDUCAODADOS.M with the given input arguments.
%
%      REDUCAODADOS('Property','Value',...) creates a new REDUCAODADOS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ReducaoDados_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ReducaoDados_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ReducaoDados

% Last Modified by GUIDE v2.5 23-Feb-2017 16:52:31

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ReducaoDados_OpeningFcn, ...
                   'gui_OutputFcn',  @ReducaoDados_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ReducaoDados is made visible.
function ReducaoDados_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ReducaoDados (see VARARGIN)

% Choose default command line output for ReducaoDados
handles.output = hObject;
set(handles.axes1,'Visible','on');
axes(handles.axes1);grid on;
% im=imread('imagem.png');
% axes('position',[0.35,0.35,0.45,0.45])
% imshow(im)
set(handles.axes2,'Visible','off');
set(handles.axes3,'Visible','off');
set(handles.text4,'String','status:'); 
set(handles.text8,'String','ALARMES:');
set(handles.text7,'String','aquisi��o(s):');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ReducaoDados wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ReducaoDados_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uiload







function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load porta;
% a = get(handles.pushbutton2,'value')
plotaVetorTimeline(portaSelecionada{1}, handles);
delete('porta.mat');
% [s, flag] = inicializaComSerial(portaSelecionada{1},handles);


function [s, flag] = inicializaComSerial(portaCom, handles)
%inicializa a comunica��o com a porta serial entre Arduio e Matlab. Ele
%garante que esta ocorrendo a comunica��o entre arduino e Matlab e o Matlab
%pode come�ar a receber/transmitir dados. Deve ser usada antes de qualquer
%programa que a interface entre os dois sistemas exista.
%ENTRADA: endere�o da porta COM
%SAIDA: 1 quando a comunica��o esta completa e 0 caso contr�rio
flag = 1;
s = serial(portaCom);
set(s,'DataBits', 8);
set(s,'StopBits', 1);
set(s,'BaudRate', 9600);
set(s,'Parity', 'none');
fopen(s);
a = 'b';
while (a ~= 'a')
    a = fread(s,1,'uchar');
end
if (a == 'a')
    disp('lendo porta serial');
    set(handles.text4,'String','status: lendo porta serial.'); 
    set(handles.text4,'String','status: lendo porta serial..'); 
    set(handles.text4,'String','status: lendo porta serial...'); 
end
fprintf(s,'%c','a');
set(handles.text4,'String','status: comunica��o serial estabelecida.');
mbox = msgbox('comunica��o serial estabelecida.');uiwait(mbox);
fscanf(s,'%c');




% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selecionado = get(hObject,'Value');
lista = get(hObject,'String');
numeroPlots = lista{selecionado};
handles.parametosParaPlotar = numeroPlots;
% axes(handles.axes2);
% p2 = plot(rand(10,1));
% axes(handles.axes3);
% p1 = plot(rand(10,1));
switch numeroPlots
    case ''
        caixaMensagem = msgbox(['selecione a quantidade de par�metros /n que' ...
        'deseja visualizar']);uiwait(caixaMensagem);
    case '1'
        set(handles.axes1,'Visible','on'); axes(handles.axes1);grid on;
        set(handles.axes2,'Visible','off');
        set(handles.axes3,'Visible','off');
    case '2'
        set(handles.axes1,'Visible','off');
        set(handles.axes2,'Visible','on'); axes(handles.axes2);grid on;
        set(handles.axes3,'Visible','on'); axes(handles.axes3);grid on;
end
handles.flagEncerrar = 'false';
guidata(hObject, handles);
        


% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fechaSerial(hObject,handles);


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Hint: get(hObject,'Value') returns toggle state of togglebutton2

function plotaVetorTimeline(portaCOM,handles)

if(~exist('flag','var'))
    potenciometro = inicializaComSerial(portaCOM,handles);
end

buf_len = 100;
dadosGravados=[];
index = 1:buf_len;
dados = zeros(buf_len,3);
flagEncerrar = 0;
while (flagEncerrar == 0)
    flagEncerrar = conexaoPorta;
    if flagEncerrar == 0
        dado = leDados(potenciometro);
        cla;
        dadosGravados = [dadosGravados; dado];
        dados = [dados(2:end,:);dado];
        plotar(handles,dados,index);
        seletorPausa = get(handles.togglebutton2, 'Value');
        while (seletorPausa == 1)
           flagEncerrar = conexaoPorta;
           if flagEncerrar == 0
               seletorPausa = get(handles.togglebutton2, 'Value');
               plotar(handles,dados,index);%Matlab apparently is not able to get the status update from "stop callback", perhaps because too busy with simulating the intensive "simulation_loop" called from "run_Callback".
    % 
    % However I found a trick to give Matlab time to check stop_Callback updates.
    % The code example above works if a statement like: pause(0.000001) is placed among run_Callback instructions.
    % 
    % (I found this trick in another thread, not my idea; at least in my case it works)
    %      drawnow
    % Instead of "pause(0.0001)", use "drawnow;". "pause" command is extremely slow because it uses very inaccurate timing mechanism. If you're running a time-critical loop, NEVER use "pause" in your loop. Having said that, drawnow is not a perfect choice either. However, there is no delay. Only the overhead to process "drawnow" exists. So, if you're not running many GUI objects doing this and that, "drawnow" exits fairly well. In a very fast computer you may finish "drawnow" within 0.1ms, which is good for usual task.
        
           end
        end
    end
end
save('PARAM.mat','dadosGravados');

function plotar(handles, dados, index)

popUpString = get(handles.popupmenu1,'string');
popUpPosicao = get(handles.popupmenu1,'value');
quantidadeParametros = popUpString(popUpPosicao);
quantidadeParam = quantidadeParametros{1};
switch quantidadeParam
    case ''
        set(handles.text8,'String',['ALERTA: Selecione o n�mero de par�metros que'...
                 ' deseja visualizar'],'FontSize',12,'HorizontalAlignment',...
                 'center', 'SelectionHighLight','on','FontWeight',...
                 'bold');
    case '1'
        axes(handles.axes1);
        plot(index, dados(:,2),'b');
        grid on;
        xlim([0 100]);ylim([ 0 1200]);
        xlabel('amostras'); ylabel('param1')
        drawnow;
        parametros(dados,handles)
    case '2'
        axes(handles.axes2);
        plot(index, dados(:,2),'b');
        grid on;
        xlim([0 100]);ylim([ 0 1200]);
        ylabel('param1')
        axes(handles.axes3);
        plot(index, dados(:,3),'b');
        grid on;
        xlim([0 100]);ylim([ 0 1200]);
        xlabel('amostras'); ylabel('param2')
        drawnow;
        parametros(dados,handles)
end


% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
while ~isempty(instrfind);
    caixaMensagem = warndlg(sprintf('Feche a porta COM antes \n de fechar o programa!'),'ALERTA');
    set(handles.text8,'String','ALERTA: FECHE A COM');
    uiwait(caixaMensagem);
    return;
end
delete(hObject);
function fechaSerial(hObject,handles)

if ~isempty(instrfind);
    fclose(instrfind);
    delete(instrfind);
end
set(handles.text4,'String','status: porta serial fechada'); 
disp('Porta Serial Fechada');

guidata(hObject, handles);

function flagEncerrar = conexaoPorta

if ~isempty(instrfind);
    flagEncerrar = 0;
else
    flagEncerrar = 1;
end
function parametros(dados,handles)
set(handles.text13,'string',dados(end,1));
deltaTempo = dados(end,1)-dados(end-1,1);
frequenciaInstantanea = 1/deltaTempo;
set(handles.text15,'string',frequenciaInstantanea);
deltaTempoMedio = dados(end,1)-dados(end-99,1);
frequenciaMedia = 100/deltaTempoMedio;
set(handles.text17,'string',frequenciaMedia);
set(handles.text19,'string',deltaTempoMedio);


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ano = input('entre com o ano do ensaio: ');
mes = input('entre com o m�s do ensaio: ');
dia = input('entre com o dia do ensaio: ');
UTC = dadosGravados(:,38);
horaUTC = fix(UTC/3600000);
minutoUTC = fix(rem(UTC,3600000)/60);
segundoUTC = (rem (UTC,3600000)/60)-minutoUTC;
serialDateNum = datenum(ano, mes, dia, horaUTC, minutoUTC, segundoUTC);
horaEixoOx = datestr(serialDateNum,13);

%plot de dados
plot(dadosGravados(:,38), dadosGravados(:,14));
% set(gca,'xticklabel',horaEixoOx)
figure();
plot(dadosGravados(:,14));
set(gca,'xticklabel',horaEixoOx)


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
